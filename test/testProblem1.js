const findingCarWithId = require ("../problem1.js") 
const inventory = require("../cars.js");

// Enter the id of the car you want to find
let id =7; 

// Calling function
const result = findingCarWithId(inventory,id)


if (result.length === 0)
{
    console.log(result)
}
else {
    const resultString = `Car ${result.id} is a ${result.car_make} ${result.car_model} ${result.car_year}`
    console.log({resultString})
    
}
