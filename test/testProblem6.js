const filteringBMWandAudiCars = require ("../problem6.js");
const inventory = require("../cars.js");

// Filtering cars based on car_year

const filteredCarsList = filteringBMWandAudiCars(inventory);

// Converting car list into JSON format

const stringifiedCarLists = JSON.stringify(filteredCarsList);

console.log(stringifiedCarLists);
