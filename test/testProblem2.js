const findingLastCarInInventory = require ("../problem2.js")
const inventory = require("../cars.js");


// Finding the last car in the inventory
const result = findingLastCarInInventory(inventory);

if (result.length === 0)
{
    console.log(result)
}
else {
    const resultString = `The last car is a ${result.car_make} ${result.car_model}`
    console.log({resultString})
    
}