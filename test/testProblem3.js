const sortCarModelsName = require ("../problem3.js") 
const inventory = require("../cars.js");

// Sorting car model name alphabetical order

const sortedList = sortCarModelsName(inventory);

//Logging the sortedList in console
console.log(sortedList);
