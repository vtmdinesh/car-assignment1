const sortCarModelOnYear= (inventory=[]) => {
    let selectedCar;
    let carYearsList =[];



    if ( (inventory.length === 0) || typeof(inventory) !== "object" ) {
        return []
    }
    else {
        // Getting car years in an array 
        for (let i = 0;i < inventory.length; i++) {
            selectedCar = inventory[i];
            carYearsList.push(selectedCar.car_year);
           
        }
    }

    // Returning filtered car years list
    return carYearsList;

}


module.exports = sortCarModelOnYear;