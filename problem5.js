
const filteringCarsOlderThan2000= (inventory=[],yearsList=[]) => {
    
    // finding number of cars made before the year 2000
    const filteredYears = []

    if ( (inventory.length === 0) || typeof(inventory) !== "object" || yearsList.length ===0 || typeof(yearsList) !== "object" )  {
        return []
    }

    else {

        for (let i=0;i<yearsList.length;i++){
            if (yearsList[i] < 2000) {
        
                filteredYears.push(yearsList[i])
            }
        }

 
    
    // Finding the list of cars made before the year 2000
    let selectedCar;
    let filteredCarList =[];

    // Getting filtered cars list in an array 

    for (let i = 0;i < inventory.length; i++) {
        selectedCar = inventory[i];
        if (selectedCar.car_year < 2000) {

            filteredCarList.push(selectedCar);
      
        }
            
    }
    
    // Returning filtered cars list
      return filteredCarList;

    }
}

module.exports = filteringCarsOlderThan2000