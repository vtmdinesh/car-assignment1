const filteringBMWandAudiCars= (inventory = []) => {
    let selectedCar;
    let filteredList =[];

    // Getting filtered cars list in an array 


    if ( (inventory.length === 0) || typeof(inventory) !== "object" ) {
        return []
    }
    else {

        for (let i = 0;i < inventory.length; i++) {
            selectedCar = inventory[i];
      
            if ((selectedCar.car_make === "BMW") || (selectedCar.car_make === "Audi" )){
        
                filteredList.push(selectedCar);
        
            }
        
        }
    // Returning filtered list of cars
        return filteredList;
    }
}


module.exports = filteringBMWandAudiCars;