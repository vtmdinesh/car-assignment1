const findingCarDetailsWithId = (inventory=[],id=undefined) => {
    
    let carDetails;
    let selectedCar = []
   

    if (inventory===[] || id === undefined ){
        return []
    }
    else {
   
        for (let i = 0;i < inventory.length; i++) {
            carDetails = inventory[i];
            if(carDetails.id === id){
                selectedCar = carDetails 
            }
        }
    }   

    if (selectedCar !== undefined){

        return selectedCar;
    }
    else {
        return []
    }    

}



module.exports = findingCarDetailsWithId